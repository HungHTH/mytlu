import 'package:flutter/material.dart' show Color;

class Course {
  final String title, topText, title1, description, iconSrc, link, time;
  final Color color;

  Course({
    required this.title,
    required this.title1,
    required this.time,
    required this.iconSrc,
    this.description = 'Build and animate an iOS app from scratch',
    this.link = "https://thanglong.edu.vn/",
    this.topText = "",
    this.color = const Color(0xFF7553F6),
  });
}

final List<Course> courses = [
  Course(
    title: "Hội Trường Dragon land",
    time: "DAY 15 - MONTH 12",
    iconSrc: "assets/icons/dragonland.png",
    title1:
        '  Hãy chuẩn bị sẵn sàng đến với sự kiện lớn nhất năm tại TLU: Hội Trường 2022. Đánh dấu sự trở lại sau 3 năm, Hội Trường năm nay sẽ có tên là Dragon Land.',

  ),
  Course(
    title: "Chào Tân Sinh Viên Morpheus 2022",
    iconSrc: "assets/icons/tlu.png",
    color: const Color(0xFF80A4FF),
    time: "DAY 23 - MONTH 11",
    title1:
        '  Morpheus là vị thần giấc mơ trong thần thoại Hy Lạp. Thông qua cái tên Morpheus, BTC muốn gửi lời chúc mừng các em tân sinh viên đã đạt được "giấc mơ TLU", bước vào cánh cửa giảng đường đại học và cánh cửa này cũng sẽ là bước đầu để chinh phục những ước mơ của chính bản thân các em trong tương lai.',
  ),
];

final List<Course> recentCourses = [
  Course(
    title: "Trang chủ",
    title1: 'Tổng quan về Thăng Long', time: '', iconSrc: '',
  ),
  Course(
    title: "Elearning",
    color: const Color(0xFF9CC5FF),
    iconSrc: "assets/icons/code.svg",
    title1: 'Tham gia học tập trực tuyến',
    link: 'https://elearning.thanglong.edu.vn/my/', time: ''
  ),
  Course(
    title: "Đăng ký học",
    title1: 'Sinh viên đăng ký học trực tuến',
    link: 'https://dangkyhoc.thanglong.edu.vn/', time: '', iconSrc: '',
  ),
  Course(
    title: "Tài liệu",
    color: const Color(0xFF9CC5FF),
    iconSrc: "assets/icons/code.svg",
    title1: 'Kho tài liệu TLU', time: '',link: "https://thuvien.thanglong.edu.vn/"
  ),
];
