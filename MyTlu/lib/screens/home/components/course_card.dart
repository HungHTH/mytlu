import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../detail_course.dart';


class CourseCard extends StatelessWidget {
  const CourseCard({
    Key? key,
    required this.title,
    required this.time,
    required this.title1,
    required this.iconSrc,
    this.color = const Color(0xFF7553F6),
  }) : super(key: key);

  final String title, iconSrc, title1, time;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        height: 280,
        width: 260,
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage(iconSrc), fit: BoxFit.cover),
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 6, right: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: Theme.of(context).textTheme.titleLarge!.copyWith(
                          color: Colors.white, fontWeight: FontWeight.w600),
                      maxLines: 2,
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 12, bottom: 8),
                    //   // child: Text(
                    //   //   title1,
                    //   //   style: Theme.of(context).textTheme.titleLarge!.copyWith(
                    //   //       color: Colors.white38, fontWeight: FontWeight.w400),
                    //   //   maxLines: 5,
                    //   // ),
                    // ),

                    Spacer(),
                    Text(time,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w500)),
                  ],
                ),
              ),
            ),
            SvgPicture.asset(iconSrc),
          ],
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) =>DetailCoursePage(image: iconSrc,title: title,title1: title1,)),
        );
      },
    );
  }
}
