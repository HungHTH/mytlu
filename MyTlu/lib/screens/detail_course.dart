import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class DetailCoursePage extends StatelessWidget {
  final String image;
  final String title;
  final String title1;

  const DetailCoursePage(
      {Key? key,
      required this.image,
      required this.title,
      required this.title1})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0; // 1.0 means normal animation speed.
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Colors.purple,
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
              padding: const EdgeInsets.all(15),
              child: Text(
                title1,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              )),
          Container(
            padding: const EdgeInsets.all(15),
            child: Hero(
                tag: image,
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(30)),
                  child: Image(
                    image: AssetImage(image),
                    fit: BoxFit.cover,
                  ),
                )),
          ),
          Container(
              padding: const EdgeInsets.all(15),
              child: Text(
                title1,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              )),
          Container(
              padding: const EdgeInsets.all(15),
              child: Text(
                title1,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              )),
          Container(
              padding: const EdgeInsets.all(15),
              child: Text(
                title1,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              )),
          Container(
              padding: const EdgeInsets.all(15),
              child: Text(
                title1,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              )),
        ]),
      ),
    );
  }
}
