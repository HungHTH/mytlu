import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewExample extends StatefulWidget {
  final String url;

  WebViewExample(this.url);

  @override
  WebViewExampleState createState() => WebViewExampleState();
}

class WebViewExampleState extends State<WebViewExample> {
  late WebViewController _webViewController;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    loading = true;
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (await _webViewController.canGoBack()) {
            _webViewController.goBack();
            return false;
          } else
            return true;
        },
        child: SafeArea(
          child: Stack(
            children:[  WebView(
                  javascriptMode: JavascriptMode.unrestricted,
                  initialUrl: widget.url,
                onPageFinished: (finish){
                  setState(() {
                    loading=false;
                  });
                },
                  onWebViewCreated: (WebViewController controller) {
                    _webViewController = controller;
                  },

              ),

              loading == true
                  ? const Center(child: CircularProgressIndicator(
                backgroundColor: Colors.cyanAccent,
                valueColor:  AlwaysStoppedAnimation<Color>(Colors.red),
              ),)
                  :  const SizedBox(),
            ]
          ),
        ),
      );
  }
}
